package com.example.restapi_unittest.repository


import com.example.restapi_unittest.entity.Customer
import org.assertj.core.api.Assertions.assertThat

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.junit.jupiter.SpringExtension


@ExtendWith(SpringExtension::class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CustomerRepositoryTest {
    @Autowired
    var entityManager : TestEntityManager? =null   //lateinit ไว้ใส่ค่าที่หลัง  ถ้าจะกำหนดค่าเลยก็ให้ต่อท้ายด้วย ? = null

    @Autowired
    var memberRepository : CustomerRepository? =null

    @AfterEach
    //@Throws(Exception::class)
    fun tearDown(){
        memberRepository?.deleteAll()
    }

    @Test
    //@Throws(InterruptedException::class)
    fun testFindByName(){

        val member = Customer()
        member.setName("Cherprang")
        entityManager?.persist(member)

        //Act
        val memberOptional = memberRepository?.findByName("Cherprang")

        //Assert
        println(memberOptional?.get(0)?.getName())
        assertThat (memberOptional?.get(0)?.getName()).isEqualTo("Cherprang")
    }


}