package com.example.restapi_unittest.repository


import com.example.restapi_unittest.entity.Customer
import org.springframework.data.jpa.repository.JpaRepository

interface CustomerRepository  :JpaRepository<Customer,Long>{
    //fun FindByName(Name :String):List<Member>
     abstract fun findByName(name: String):List<Customer>

}
