package com.example.restapi_unittest.entity

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.*

@Entity
//@Data
//@AllArgsConstructor
//@NoArgsConstructor

@Table(name = "Unittest")
class Customer {
    //constructor(): this("")
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    var id : Long =0
    private var name : String?=null

    fun setName(Name : String){
        this.name = Name
    }
    fun getName():String?{
        return name
    }

}