package com.example.restapi_unittest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RestapiUnittestApplication

fun main(args: Array<String>) {
	runApplication<RestapiUnittestApplication>(*args)
}
